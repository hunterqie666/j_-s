public class CSDN {
    public static void main(String[] args) {
        boolean a=true;
        boolean b=false;

        System.out.println(a);//true
        System.out.println(b);//false

        //a=1;
    }
    public static void main7(String[] args) {
        char a='A';
        char b='1';
        char c='帅';

        System.out.println(a);//A
        System.out.println(b);//1
        System.out.println(c);//帅

    }
    public static void main6(String[] args) {
        float a=2.12f;//这里后面要带f表示float类型
        System.out.println(a);

        System.out.println(Float.MIN_VALUE);//最小值：1.4E-45
        System.out.println(Float.MAX_VALUE);//最大值：3.4028235E38

    }
    public static void main5(String[] args) {
        double a=2.12;
        System.out.println(a);

        System.out.println(Double.MAX_VALUE);//最大值：1.7976931348623157E308
        System.out.println(Double.MIN_VALUE);//最小值：4.9E-324

        System.out.println(a*a);//4.494400000000001

    }
    public static void main4(String[] args) {

        byte a = 10;
        System.out.println(a);


        System.out.println(Byte.MIN_VALUE);//最大值：-128
        System.out.println(Byte.MAX_VALUE);//最小值：127

    }
    public static void main3(String[] args) {
        short a=10;
        System.out.println(a);

        System.out.println(Short.MAX_VALUE);//最大值:32767
        System.out.println(Short.MIN_VALUE);//最小值：-32768

        //a=32768;
    }
    public static void main2(String[] args) {

        long a = 10; // long定义的长整型变量
        System.out.println(a);

        long b = 10L; // 为了区分int和long类型，一般建议：long类型变量的初始值之后加L或者l


        System.out.println(Long.MIN_VALUE);//最大值：-9223372036854775808
        System.out.println(Long.MAX_VALUE);//最小值：9223372036854775807

        //b = 9223372036854775808;

    }
    public static void main1(String[] args) {
        int a=10;
        System.out.println(a);

        int b;//在定义时没有给初始值，但使用前必须设置初值
        b=20;
        System.out.println(b);

        System.out.println(Integer.MAX_VALUE);//最大值 2147483647
        System.out.println(Integer.MIN_VALUE);//最小值 -2147483648
        //Ineteger是包装类 这里可以暂时理解为int的plus版本 后续会进行讲解

        //a=2147483648;
    }
}
