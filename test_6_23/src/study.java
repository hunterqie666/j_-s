public class study {
    public static void main(String[] args) {
        boolean flag =(true==true) ? (true==true)?false:true : false;//使用
//布尔表达式1？表达式2：表达式3 使用的方式结果和C语言类似 只不过一个是0/1表达真假，一个数true和false表达真假
        System.out.println((true==true) ? (true==true)?false:true : false);
        //第一个(true==true)是布尔表达式 为真执行(true==true)?false:true
        //布尔表达式1？：(true==true) 表达式2： (true==true)?false:true  表达式三：false
    }
    public static void main8(String[] args) {
        int a=1;
        int b=2;
        //>>> 无符号右移 统统在左边补0 无符号都是默认都是正数。
        //<<   >>  位移操作符 其使用的方式结果和C语言类似（二进制）
        //对于正数来说<<操作符是十进制数字*2 >>操作符是十进制数字/2
        //<<是补0  >>是补符号位

        //~ 按位取反 其使用的方式结果和C语言类似（二进制）
        //^ 按位异或 其使用的方式结果和C语言类似（二进制）
        //| 按位或   其使用的方式结果和C语言类似（二进制）
        //& 按位与   其使用的方式结果和C语言类似（二进制）

        //& | 两边如果都是布尔型，就会把其当成逻辑操作符（&& ||）-->true false
        System.out.println(a==1&a==2);
        System.out.println(a==2&(1/0)==1);//表达式1如果是false 表达式2依然会执行
        //不能进行短路求值了
    }
    public static void main7(String[] args) {
        int a= 1;
        int b =1;
        //System.out.println(a&&b);// 报错 两边要是布尔表达式-->输出结果要是true和false
        //&& 逻辑与（短路与）
        // 其使用的方式结果和C语言类似 只不过一个是0/1表达真假，一个数true和false表达真假
        System.out.println(a==1&&b==1);
        System.out.println(a==2&&b==2);

        //|| 逻辑与 其使用的方式结果和C语言类似 只不过一个是0/1表达真假，一个数true和false表达真假
        //两边要是布尔表达式-->输出结果要是true和false
        System.out.println(a==1||b==2);
        System.out.println(a==2||b==1);

        //! 逻辑非 其使用的方式结果和C语言类似 只不过一个是0/1表达真假，一个数true和false表达真假
        // 右边要是布尔表达式-->输出结果要是true和false
        System.out.println(!(a==1));
    }
    public static void main6(String[] args) {
        System.out.println(1<5);
        System.out.println(1!=5);
        System.out.println(1>5);
        //结果只有true和false

        int a =10;
        //System.out.println(1<a<20);//报错 返回的是true和false
    }
    public static void main5(String[] args) {
        short sh=10;
        //sh=sh+2;
        //会报错，因为2是整型，整型在运算的时候会提升到四个字节。
        //然后计算之后把值还给sh 会导致数据的丢失。
        sh+=2;//可以理解为 把类型进行了转换。

    }
    public static void main4(String[] args) {
        System.out.println(11.5%2);//这里是可以求出来的-->1.5

        System.out.println(10%3);//1
        System.out.println(-10%3);//-1
        System.out.println(10%-3);//1
        System.out.println(-10%-3);//-1
        //简便来看就是看前面的被除数是否为负数。

        System.out.println(5/2);//2

        System.out.println((float)5/2);//2.5
        System.out.println(5/(float)2);//2.5
        //5.0/2-->小数部分就可以接收  5/2.0-->同理

        System.out.println((float)(5/2));//2/0
    }
    public static void main3(String[] args) {
        String str ="123";
        String str1 ="123";
        int b =Integer.parseInt(str);
        System.out.println(str);
        int a= Integer.valueOf(str);//value是调用parseint parseint的是value的底层。
        System.out.println(a+1);
        //字符串里面的是数字才能转，字母不能。
    }
    public static void main2(String[] args) {
        int a=123;
        String ret1=a+" ";//利用字符串+基本数据类型==字符串
        String ret = String.valueOf(a);//通过.号来调用类当中的方法
        System.out.println(ret);
        System.out.println(ret1);
    }
    public static void main1(String[] args) {
        String str1="hello";
        String str2="world";
        System.out.println(str1);//hello
        System.out.println(str1+str2);//helloworld
        //字符串+字符串是两个字符串直接相加
        int a=10,b=20;
        System.out.println(a+b);//30
        System.out.println("a ="+ a);//a=10
        System.out.println("b ="+ b);//b=20
        System.out.println("a + b ="+ a+b);//1020
        System.out.println("a + b ="+ (a+b));//30
        System.out.println(a+b+"是a+b的和");//30是a+b的和
        //String是+其他的数据类型还是字符串
    }
}
