import java.util.Arrays;
import java.util.Scanner;

public class Study {
    public static void main13(String[] args) {
        int[] arr1 = {1,2,4,6,5,10,8};

        //方法一
        //通过下标去打印数组 数组名 + . + length == 数组的元素个数
        for (int i = 0; i < arr1.length; i++) {
            System.out.print(arr1[i] + " ");
        }

        System.out.println();
        System.out.println("----------------------");

        //方法二
        // 数据类型化 + 自己给的名字 +  ： + 数组名 ——>打印数组的各个元素
        for(int x : arr1) {
            System.out.print(x + " ");
        }

        System.out.println();
        System.out.println("----------------------");

        //方法三 Java自带的方法 用来打印数组元素
        String str = Arrays.toString(arr1);
        System.out.println(str);
    }
    public static void main12(String[] args) {
        int[] arr1 = {1,2,4,5,6};

        int[] arr2 = new int[10];
        int[] arr3 = new int[]{1,2,4,5};

        /*int[] arr4 = new int[10]{1,2,3};
        int[] arr5 = new int[4]{1,2,4,5};*/
        //这两种都是错的 []里面不能加数字
    }
    public static int fun3(int n){
        if(n  <= 9 ){
            return n;
        }
        return n % 10 + fun3(n / 10);
    }
    public static void main10(String[] args) {
        System.out.println(fun3(123));
    }
    public static int sum1(int n) {
        if (n == 1) {
            return 1;
        }
        return n+sum1(n-1);
    }

    public static void main9(String[] args) {
        System.out.println(sum1(10));
    }
    public static void fun1(int n ){
        if(n  <= 9 ){
            System.out.println(n);
            return;
        }
        fun1(n / 10);
        System.out.println(n % 10);
    }

    public static void main8(String[] args) {
        fun1(1234);
    }
    public static int fun(int n) {
        if (n == 1) {
            return 1;
        }
        int tmp = n * fun(n-1);
        return tmp;
    }

    public static void main7(String[] args) {
        System.out.println(fun(5));
    }
    public static int fib (int n ){
        if(n == 1 || n == 2 ) {
            return 1;
        }
        int f1 = 1,f2 = 1,f3 = 0;
        for (int i = 3; i <= n ; i++) {
            f3 = f1 + f2;
            f1 = f2;
            f2 = f3;
        }
        return f3;
    }
    public static void main6(String[] args) {
        System.out.println(fib(40));
    }
    public static void main5(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 3;
        while (count != 0) {
            String str = scanner.nextLine();
            if (str.equals("123456")) {
                System.out.println("登陆成功");
                break;
            } else {
                count--;
                System.out.println("登录失败");

            }
        }
    }
    public  static double fun() {
        double sum = 0;
        int a = 1;
        for (int i = 1; i <=100 ; i++) {
            sum += 1.0 / i * a;//如果这边是1的话 相/ 就是小数 两个整数向下兼容--> 0
            a = -a;
        }
        return sum;
    }
    public static void main4(String[] args) {
        double d = fun();
        System.out.println(d);
    }

    public static void swap(int a , int b) {
        int tmp = 0;
        tmp = a;
        a = b;
        b = tmp;
        System.out.println(a);
        System.out.println(b);
    }
    public static void main3(String[] args) {
        int a = 10;
        int b = 20;
        swap(a,b);
    }


    public static int Math(int a) {
        int sum = 0;
        for (int i = 1; i <=a ; i++) {
            int ret = 1;
            for (int j = 1; j <= i; j++) {
                ret *= j;
            }
            sum += ret;
        }
        return sum;
    }
    public static void main2(String[] args) {
      int add = Math(5);
        System.out.println(add);
    }


    public static int add(int a,int b) {
        return a + b;
    }
    public static void main1(String[] args) {
       int a =10,b = 20;
       int sum = add( a , b);
        System.out.println(sum);
    }
}