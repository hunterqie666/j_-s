import java.util.Random;
import java.util.Scanner;
public class Study {
    public static void main(String[] args) {
        //乘法口诀表
        Scanner ch = new Scanner(System.in);
        int a = ch.nextInt();
        int i = 0;
        for (i = 1; i <= a; i++)
        {
            int j = 1;
            for (j = 1; j <= i; j++)
            {
                System.out.printf("%d*%d=%-2d  ", i, j, i * j);
            }//%和d之间2的话是两位向右对齐，-2就向左对齐9

            System.out.println();
        }
    }
    public static void main9(String[] args) {
        Scanner ch = new Scanner(System.in);
        int a = ch.nextInt();
        while (a != 0) {
            System.out.print(a % 10+" ");
            a /= 10;
        }
    }
    public static void main8(String[] args) {
        Scanner ch = new Scanner(System.in);
        int b = ch.nextInt();
        int count = 0;
        while (true) {
            int a = ch.nextInt();
            if (a == b) {
                System.out.println("登录成功");
                break;
            } else if (a != b) {
                count++;
                if (count == 3) {
                    break;
                }
                System.out.println("密码错误 重新输入");
            }
        }

    }

    public static void main7(String[] args) {
        //给定两个数，求这两个数的最大公约数
        Scanner ch = new Scanner(System.in);
        int a = ch.nextInt();
        int b = ch.nextInt();
        int c = a % b;
        while (c != 0) {
            a = b;
            b = c;
            c = a % b;
        }
        System.out.println(b);
    }
    public static void main6(String[] args) {
        //计算1/1-1/2+1/3-1/4+1/5 …… + 1/99 - 1/100 的值
        double sum = 0.0;
        int flag = 1;
        for (int i = 1; i <= 100; i++) {
            sum += flag*1.0/i;
            flag = -flag;
        }
        System.out.println(sum);
    }
    public static void main5(String[] args) {
        //猜数字小游戏
        Scanner ch = new Scanner(System.in);
        Random math = new Random();//生成随机数
        int a = math.nextInt(100);//生成一个0-99的数字 存在a里面

        while (true) {
            System.out.println("请输入你要猜的数字：");
            int b = ch.nextInt();
            if(b > a){
                System.out.println("猜大了！");
            }else if(b == a){
                System.out.println("猜对了！");
                break;
            }else{
                System.out.println("猜小了！");
            }
        }
    }
    public static void main4(String[] args) {
        //打印X
        Scanner ch = new Scanner(System.in);
        while (ch.hasNextInt()) {
            int a = ch.nextInt();
            for (int i = 0; i < a; i++) {//循环输入
                for (int j = 0; j < a; j++) {
                    if(i == j || i+j == a-1){
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        }
    }
    public static void main3(String[] args) {
        //求一个二进制数字的奇数位和偶数位的数字 分别打印出来
        //从右往左数 右边第一个数是0
        Scanner ch = new Scanner(System.in);
        int a = ch.nextInt();
        //-=2 奇数位和偶数位是相互穿插
        //最高位数符号位+32位机器+奇数位的数字-->所以就是31开始，1为结束
        for (int i = 31; i >= 1; i-=2) {
            System.out.print(((a >> i) & 1)+" ");
        }
        System.out.println();
        //最高位数符号位+32位机器+偶数位的数字-->所以就是30开始，0为结束
        for (int i = 30; i >=0 ; i-=2) {
            System.out.print(((a >> i) & 1)+" ");
        }
    }

    public static void main2(String[] args) {
        //求二进制数字中1的个数
        Scanner ch = new Scanner(System.in);
        int a = ch.nextInt();
        int count = 0;
//第三种
        //一个数一直&比他小1的数 可以知道1
        while (a != 0) {
            a = a & (a - 1);
            count++;
        }

//第二种
        //当a为0的时候直接就不算了
        //提高代码效率
        /*while (a != 0) {
            if ((a & 1) != 0) {
                count++;
            }
            a = a >>> 1;//解决负数问题 无符号右移
        }*/
//第一种
        /*
        //当右移时 1和1相& 全1为1 当某位是0的时候if就进不去
        for (int i = 0; i < 32; i++) {//32位机器
            if(((a >>> i) & 1) != 0) {
                count++;//每次右移一位就是
            }
        }*/
        System.out.println(count);
    }
    public static void main1(String[] args) {
        //输入一个数 判断再其范围内的水仙花数
        Scanner ch = new Scanner(System.in);
        int a = ch.nextInt();

        for (int i = 0; i <=a ; i++) {
            int count = 0;
            int tmp = i;
            //为了避免i的改变 于是赋值给tmp
            while (tmp != 0) {
                count++;//知道i是几位数
                tmp /= 10;
            }
            int sum = 0;
            tmp = i;
            while(tmp !=0 ) {
                sum += Math.pow(tmp % 10,count);//每一位的count次方相加
                tmp /= 10;
            }
            //判断是否相等
            if(sum == i) {
                System.out.println(i);
            }
        }
    }
}