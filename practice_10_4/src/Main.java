import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num1 = in.nextInt();
        int num2 = in.nextInt();
        int[][] arr = new int[num1][num2];

        for (int i = 0; i < num1; i++) {
            for (int j = 0; j < num2; j++) {
                int z = in.nextInt();
                arr[i][j] = z;
            }
        }

        int z = 0;
        int k = 0;
        int MAX = arr[0][0];

        for (int i = 0; i < num1; i++) {
            for (int j = 0; j < num2; j++) {
                if (MAX < arr[i][j]){
                    MAX = arr[i][j];
                    z = i;
                    k = j;
                }
            }
        }
        z++;
        k++;
        System.out.print(z + " " + k);
    }
    public static void main5(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] arr = new int[4];
        for (int i = 0; i < 4; i++) {
            int z = in.nextInt();
            arr[i] = z;
        }
        int MAX = arr[0];
        for (int i = 1; i < 4; i++) {
            if(MAX < arr[i]){
                MAX = arr[i];
            }
        }
        System.out.println(MAX);
    }

    public static void main4(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[][] arr= new int[n][3];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                int z = in.nextInt();
                arr[i][j] = z;
            }
        }
        int sum = 0;
        int count = 0;
        for (int i = 0; i < n; i++) {
            sum = 0;

            for (int j = 0; j < 3; j++) {
               sum += arr[i][j];
            }

            sum /= 3;
            if(sum < 60){
                count++;
            }

        }
        System.out.println(count);
    }
    public static void main3(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        int averger = (a + b + c) / 3;
        if( averger >= 60){
            System.out.println("NO");
        }else {
            System.out.println("YES");
        }
    }
    public static void main2(String[] args) {
        Scanner in = new Scanner(System.in);
        double a = in.nextDouble();
        double b = in.nextDouble();
        double c = in.nextDouble();
        double sum = a + b + c;
        double averger = sum / 3;
        System.out.printf("%.2f ",sum);
        System.out.printf("%.2f",averger);
    }
    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        int tmp = 0;
        if(a < b){
            a = b;
        }
        if(a < c){
          a = c;
        }
        System.out.println(a);
    }
}