class Undergraduate  extends Student{
    String academy;
    String department;

    public static void main(String[] args) {
        Student s = new Student();
        s.setData("张三",1);
        s.print();
        Undergraduate u = new Undergraduate();
        u.setData("李四",2);
        u.academy = "经信分院";
        u.department = "信息系";
        System.out.println(u.name + "," + u.number + "," + u.academy + "," + u.department);
    }
}
