import java.util.Scanner;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException{
        Scanner s = new Scanner(System.in);
        long n = s.nextLong();
        StringBuffer sb = new StringBuffer();
        long r = 0;//存储余数
        while(n!=0){
            r = n%6;
            n = n/6;
            sb.insert(0,r+"");
        }
        System.out.println(sb.toString());
    }

    public static void main2(String[] args) throws IOException{
        boolean judge = true;
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[][] arr = new int[n][n];
        //读入n*n方阵
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                arr[i][j] = in.nextInt();
            }
        }
        //判断方阵是否为上三角矩阵
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(i>j){
                    if(arr[i][j]!=0){
                        judge = false;
                    }
                }
            }
        }
        //判断输出结果
        if(judge){
            System.out.println("YES");
        }else{
            System.out.println("NO");
        }
    }
    public static void main1(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (scan.hasNext()) {
            int N = scan.nextInt();//行
            int M = scan.nextInt();//列
            int a[][] = new int[N][M];
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    a[i][j] = scan.nextInt();
                }
            }
            int c = scan.nextInt();
            int d = scan.nextInt();
            System.out.println(a[c-1][d-1]);
        }
    }
}

