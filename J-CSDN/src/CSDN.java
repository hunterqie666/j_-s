public class CSDN {
    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        int max = a > b ? a : b;
        //a>b是false 所以执行b-->b=20-->max=20
        System.out.println(max);
    }
    public static void main9(String[] args) {
        int a = 5;

        System.out.println(a<<1);//10
        System.out.println(a>>1);//20
        System.out.println(a>>>1);//2

    }
    public static void main8(String[] args) {
        int a = 10;
        int b = 20;

        System.out.println(a & b);//0
        System.out.println(a | b);//30
        System.out.println(a ^ b);//30

        System.out.println(~a);//-11
    }
    public static void main7(String[] args) {
        int a = 1;
        int b = 2;
        //逻辑与 &&
        System.out.println(a == 1 && b == 2); // 左为真 且 右为真 则结果为真
        System.out.println(a == 1 && b > 100); // 左为真 但 右为假 则结果为假
        System.out.println(a > 100 && b == 2); // 左为假 但 右为真 则结果为假
        System.out.println(a > 100 && b > 100); // 左为假 且 右为假 则结果为假

        //逻辑或 ||
        System.out.println(a == 1 || b == 2); // 左为真 且 右为真 则结果为真
        System.out.println(a == 1 || b > 100); // 左为真 但 右为假 则结果也为真
        System.out.println(a > 100 || b == 2); // 左为假 但 右为真 则结果也为真
        System.out.println(a > 100 || b > 100); // 左为假 且 右为假 则结果为假

        System.out.println(!(a == 1)); // a == 1 为true，取个非就是false
        System.out.println(!(a != 1)); // a != 1 为false，取个非就是true
    }
    public static void main6(String[] args) {
        int a = 10;
        int b = 20;

        System.out.println(a>b);//false
        System.out.println(a<b);//true
        System.out.println(a==b);//false
        System.out.println(a!=b);//true
        System.out.println(a>=b);//false
        System.out.println(a<=b);//true
    }
    public static void main5(String[] args) {
        int a = 10;
        int b = 0;

        b = a++;
        //先把a的值给b-->b=10；然后a本身自己 +1-->11
        System.out.println(b);//10
        System.out.println(a);//11

        //上面的 a自己++ 已经等于11了
        b = ++a;
        //先把 a+1 的值给b-->b=12；a本身这里自己+1-->12
        System.out.println(b);//12
        System.out.println(a);//12
    }
    public static void main4(String[] args) {
        int a = 10;

        a += 1;//可以想象成--> a=a+1
        System.out.println(a);//11

        a -= 2;//可以想象成--> a=a-1
        System.out.println(a);//9

        a *= 2;//可以想象成--> a=a*1
        System.out.println(a);//18

        a /= 2;//可以想象成--> a=a/1
        System.out.println(a);//9

        a %= 2;//可以想象成--> a=a%1
        System.out.println(a);//1
    }
    public static void main3(String[] args) {
        double a = 1.0;
        double b = 0.5;
        //System.out.println(a/b);//2.0

        System.out.println(1+0.2);//1.2
        //1为int类型 0.2是double类型
        //但最后结果是1.2-->1 提升成了double
    }
    public static void main2(String[] args) {
        int a = 1;
        int b = 0;
        System.out.println(a/b);
    }
    public static void main1(String[] args) {
        int a = 10;
        int b = 20;

        System.out.println(a + b);//30
        System.out.println(a - b);//-10
        System.out.println(a * b);//200
        System.out.println(a / b);//0  要的是整数部分
        System.out.println(a % b);//10 %取模相当于取的是余数部分
    }
}
