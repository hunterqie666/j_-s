public class CSDN {
    public static void main(String[] args) {
//打印 1 - 10
        int num = 1;
        do {
            System.out.println(num);
            num++;
        } while (num <= 10);


        /*do{
            循环语句;
        }while(循环条件);*/

    }
    public static void main6(String[] args) {
//计算1~100的和
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum += i;
        }
        System.out.println("sum = " + sum);

        /*for(表达式1;布尔表达式2;表达式3){
            表达式4;
        }*/
    }
    public static void main4(String[] args) {
//1-100的数字
        int a = 1;
        while (a<=100){
            System.out.println(a);
            a++;
        }

       /* while(循环条件){
            循环语句;
        }*/

    }
    public static void main3(String[] args) {
       //根据 day 的值输出星期
        int day = 1;
        switch(day) {
            case 1:
                System.out.println("星期一");
                break;
            case 2:
                System.out.println("星期二");
                break;
            case 3:
                System.out.println("星期三");
                break;
            case 4:
                System.out.println("星期四");
                break;
            case 5:
                System.out.println("星期五");
                break;
            case 6:
                System.out.println("星期六");
                break;
            case 7:
                System.out.println("星期日");
                break;
            default:
                System.out.println("输入有误");
                break;
        }

        /*switch ( 表达式 ){
            case 常量值1：{
                //语句1；
                break;
            }
            case 常量值2：{
                //语句2；
                break;
            }
            default:{
                //当表达式与常量值都不匹配的时候执行次语句
                break;
            }
        }*/
    }
    public static void main2(String[] args) {

        /*if ( 布尔语句表达式 ) {
            //语句
        }else if ( 布尔语句表达式 ) {
            //语句
        }
        else  {
            //语句
        }*/
        //判断一个数字是正数，负数，还是零
        int num = 10;
        if (num > 0) {
            System.out.println("正数");
        } else if (num < 0) {
            System.out.println("负数");
        } else {
            System.out.println("0");
        }
//---------------------------------------
        /*if ( 布尔语句表达式 ) {
            //语句
        } else  {
            //语句
        }*/
        //判断一个数字是奇数还是偶数
        int i = 10;
        if (i % 2 == 0) {
            System.out.println("num 是偶数");
        } else {
            System.out.println("num 是奇数");
        }
//--------------------------------------
        /*if ( 布尔语句表达式 ) {
            //语句
        }*/
        int a = 10;
        if (a == 10) {
            System.out.println("hunetr");
        }
    }
    public static void main1(String[] args) {
        int a = 10;

        System.out.println(a);//10
        System.out.println(++a);//11
        System.out.println(a-=1);//10
    }
}
