import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int a = in.nextInt();
            if (a == 200)
                System.out.println("OK");
            else if (a == 202)
                System.out.println("Accepted");
            else if (a == 400)
                System.out.println("Bad Request");
            else if (a == 403)
                System.out.println("Forbidden");
            else if (a == 404)
                System.out.println("Not Found");
            else if (a == 500)
                System.out.println("Internal Server Error");
            else if (a == 502)
                System.out.println("Bad Gateway");
        }
    }
    public static void main5(String[] args) {
        Scanner in = new Scanner(System.in);
         int[] arr =new int[7];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = in.nextInt();
        }
        int max = arr[0];
        int min = arr[0];
        int j = 0, k = 0;
        for (int i = 1; i < arr.length; i++) {
            if(max < arr[i]){
                j = i;
            }
            if(min > arr[i]){
                k = i;
            }
        }
        arr[k] = 0;
        arr[j] = 0;
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if( i == k || i == j ){
                continue;
            }
            sum += arr[i];
        }
        double averger = sum / 5;
        System.out.println(String.format("%.2f",averger));
    }
    public static void main4(String[] args) {
        Scanner in = new Scanner(System.in);
        double  a = in.nextDouble();
        double  b = in.nextDouble();
        double  c = in.nextDouble();
        double  d = in.nextDouble();
        double  e = in.nextDouble();
        double averger = (a + b + c + d + e) / 5;
        System.out.println(String.format("%.2f",averger));

    }
    public static void main3(String[] args) {
    Scanner in = new Scanner(System.in);
    while(in.hasNextInt()){
        int n = in.nextInt();
        if (n % 2 == 0) {
            System.out.println("Even");
        }
        else {
            System.out.println("Odd");
        }
     }
    }
    public static void main2(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()) {
            int num = in.nextInt();
            if(num>=60){
                System.out.println("Pass");
            } else {
                System.out.println("Fail");
            }
        }
    }
    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            char a = in.next().charAt(0);
            if (a >= 'A' && a >= 'Z') {
                System.out.println(Character.toUpperCase(a));
            } else if (a >= 'a' && a <= 'z') {
                System.out.println(Character.toUpperCase(a));
            }
        }
    }
}