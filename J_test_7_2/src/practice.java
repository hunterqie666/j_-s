import com.sun.org.apache.bcel.internal.generic.NEW;

import java.util.Scanner;

public class practice {
    public static void main6(String[] args) {
        //九九输入乘法口诀表
        for (int k = 1; k <= 9; k++) {
            int l = 0;
            for ( l = 1; l <k ; l++) {
                System.out.printf("%d*%d=%-3d ", k, l, k * l);
            }
            System.out.println();
        }
    }
    public static void main5(String[] args) {
        //输入一个数看是什么年龄状态
        Scanner age = new Scanner(System.in);
        int a = age.nextInt();

        if (a <=18) {
            System.out.println("少年");
        } else if (a>19&&a<=28) {
            System.out.println("青年");
        } else if (a>29&&a<=55) {
            System.out.println("中年");
        } else if (a>=56) {
            System.out.println("老年");
        }
    }
    public static void main4(String[] args) {
        //输入一个数判断是不是素数
        Scanner ch = new Scanner(System.in);
        int a = ch.nextInt();
        int i = 0;
        for (i = 2; i < a; i++) {
            if(a % i == 0) {
                System.out.println("不是素数");
                break;
            }
        }
        if(a==i) {
            System.out.println("是素数");
        }
    }
    public static void main3(String[] args) {
        //打印 1 - 100 之间所有的素数
        //素数：只能被1和本身整除
        for (int i = 1; i <=100 ; i++) {//循环1-100的数
            int j = 0;
            for (j = 2; j < i; j++) {//除数
                if(i % j == 0){
                    break;
                }
            }
            if (i == j) {
                System.out.println(i);
            }
        }
    }
    public static void main2(String[] args) {
        //输出 1000 - 2000 之间所有的闰年
        int year = 1000;
        while(year<=2000){
            if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
                System.out.println(year);
            }
            year++;
        }
    }
    public static void main1(String[] args) {
        //编写程序数一下 1到 100 的所有整数中出现多少个数字9
        for (int i = 1; i <100 ; i++) {
            if(i % 10 == 9 || i / 10 == 9){
                System.out.println(i);
            }
        }
    }
}
