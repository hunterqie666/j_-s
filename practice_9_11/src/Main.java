import java.util.Scanner;
import java.util.Random;

public class Main {
    public static void main6(String[] args){
        int sum = 0;
        for(int i = 2 ; i <= 100 ;i+=2) {
            sum += i;
        }
        System.out.println("for循环的总和：" + sum);
        sum = 0;
        int i = 2;
        while(i<=100){
            sum += i;
            i+=2;
        }
        System.out.println("while循环的总和：" + sum);

        sum = 0;
        i = 2;
        do {
            sum += i;
            i+=2;
        }while(i <= 100);
        System.out.println("do……while循环的总和：" + sum);
    }

    public static void main5(String[] args){
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
		/*if(num>=90 && num<=100) {
			System.out.println('A');
		}
		else if(num>=80 && num<=90) {
			System.out.println('B');
		}
		else if(num >= 70 && num <= 80) {
			System.out.println('C');
		}
		else if(num >= 60 && num <= 70 ) {
			System.out.println('D');
		}
		else if(num <= 60) {
			System.out.println('E');
		}
		**/
        num /= 10;
        switch(num) {
            case 10:
            case 9:
                System.out.println('A');
                break;
            case 8:
                System.out.println('B');
                break;
            case 7:
                System.out.println('C');
                break;
            case 6:
                System.out.println('D');
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                System.out.println('E');
                break;
        }
    }

    public static void main4(String[] args){
        for(int i = 0;i < 10 ; i++) {
            Random random = new Random();
            int rand = random.nextInt(100);
            System.out.print(rand + " ");
        }
    }

    public static void main3(String[] args){
        Scanner num = new Scanner(System.in);
        int n1 = num.nextInt();
        int n2 = num.nextInt();
        int len = 2 * n1 + 2 * n2;
        int area = n1 * n2;
        System.out.println("周长是：" + len);
        System.out.println("面积是：" + area);
    }

    public static void main2(String[] args){
        for(int i = 65;i <= 90 ;i++){
            System.out.printf("%c ",i);
        }
    }

    public static void main1(String[] args) {
        System.out.println("HelloWorld!");
    }
}