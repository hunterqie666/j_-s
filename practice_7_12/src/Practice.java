import java.util.Scanner;

public class Practice {
    public static void main7(String[] args) {
        int[] arr = {1,2,3,4,5,6};
        for (int i = arr.length - 1 ; i >=0 ; i--) {
            System.out.print(arr[i] + " ");
        }
    }
    public static int fib(int n) {
            if (n <= 2)
                return 1;
            else
                return fib(n - 1) + fib(n - 2);
    }
    public static void main6(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int ret = fib(n);
        System.out.println(ret);
    }
    public static int get(int m ,int n ) {
        //看两个变量2进制位相同的个数
        int co = 0;
        int tmp = m ^ n;
        while (tmp != 0) {
            tmp = tmp & (tmp - 1);
            co++;
        }
        return co;
    }
    public static void main5(String[] args) {
   //看两个变量2进制位相同的个数
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int co=get(m, n);
        System.out.println(co);
    }
    public static void main3(String[] args) {
        //打印成菱形
        Scanner scanner = new Scanner(System.in);
        int line = scanner.nextInt();//要行数必须是奇数，怕有人写成偶数，就把其分别为上半部分和下半部分
        //打印上半部分
        int i = 0;
        for (i = 0; i < line; i++) {
            //打印空格
            int j = 0;
            for (j = 0; j<line-1-i; j++) {
                System.out.print(" ");
            }
            //打印*
            for (j = 0; j<2*i+1; j++) {
                System.out.print("*");
            }
            System.out.println();
        }

        //打印下半部分
        for (i = 0; i < line - 1; i++) {
            //打印空格
            int j = 0;
            for (j = 0; j<=i; j++) {
                System.out.print(" ");
            }
            //打印*
            for (j = 0; j<2*(line-1-i)-1; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
    public static void main2(String[] args) {
        //输入三个数 比大小
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int c = scanner.nextInt();
        int b = scanner.nextInt();
        if (a < b) {
            int tmp = a;
            a = b;
            b = tmp;
        }
        if (a < c) {
            int tmp = a;
            a = c;
            c = tmp;
        }
        if (b < c) {
            int tmp = b;
            b = c;
            c = tmp;
        }
        System.out.println(a);
    }
    public static void main1(String[] args) {
      //1-100三的倍数
            int i = 0;
            for (i = 1; i <= 100; i++) {
                if (i % 3 == 0)//一定要记住/除只要整数部分
                    System.out.println(i);
            }
        }//思路:一个数跟三取模余数为0就是3的倍数
}