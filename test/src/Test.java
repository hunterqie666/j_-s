import java.util.Scanner;
public class Test {

    public static void displaySortedScores(double num1,double num2,double num3) {
        double tmp = 0;
        if (num1 < num2){
            tmp = num1;
            num1 = num2;
            num2 = tmp;
        }
        if (num1 < num3){
            tmp = num1;
            num1 = num3;
            num3 = tmp;
        }
        if (num2 < num3){
            tmp = num2;
            num2 = num3;
            num3 = tmp;
        }
        System.out.println(num1 + " " + num2 + " " + num3);
    }

    public static void sortNumbers(double num1,double num2,double num3){
        double tmp = 0;
        if (num1 > num2){
            tmp = num1;
            num1 = num2;
            num2 = tmp;
        }
        if (num1 > num3){
            tmp = num1;
            num1 = num3;
            num3 = tmp;
        }
        if (num2 > num3){
            tmp = num2;
            num2 = num3;
            num3 = tmp;
        }
        System.out.println(num1 + " " + num2 + " " + num3);
    }
    public static void main3(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double n1 = scanner.nextInt();
        double n2 = scanner.nextInt();
        double n3 = scanner.nextInt();
        //sortNumbers(n1 , n2, n3);
        displaySortedScores(n1,n2,n3);
    }

    public static double calcilator(double num1,double num2,char op){
        double num = 0;
        if( op == '+'){
            num =  num1 + num2;
        }else if( op == '-'){
            num =  num1 - num2;
        }else if( op == '*'){
            num =  num1 * num2;
        }else if( op == '/'){
            num =  num1 / num2;
        }
        return num;
    }

    public static void main2(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n1 = scanner.nextInt();
        int n2 = scanner.nextInt();
        char F =  scanner.next().charAt(0);

        double num = calcilator(n1,n2,F);
        System.out.println(num);
    }
    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int year = scanner.nextInt();

        int arr1[] = {31,28,31,30,31,30,31,31,30,31,30,31};
        int arr2[] = {31,29,31,30,31,30,31,31,30,31,30,31};
        int count = 0;

        if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)){
            for (int i = 0; i < 12; i++) {
                System.out.println("日\t一\t二\t三\t四\t五\t六");
                for (int j = 1; j <= arr2[i]; j++) {

                    if(count % 7 == 0){
                        System.out.println();
                    }
                    System.out.printf("%-2d\t",j);
                    count++;
                }
                count = 0;
                System.out.println();
                System.out.println();
            }
        }
        else {
            for (int i = 0; i < 12; i++) {
                System.out.println("日\t一\t二\t三\t四\t五\t六");
                for (int j = 1; j <= arr2[i]; j++) {

                    if(count % 7 == 0){
                        System.out.println();
                    }
                    System.out.printf("%-2d\t",j);
                    count++;
                }
                count = 0;
                System.out.println();
                System.out.println();
            }
        }
    }
}
