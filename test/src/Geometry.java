import java.util.Scanner;

public class Geometry {
    public static double calculateCircleArea(double radius) {
        if (radius < 0) {
            throw new IllegalArgumentException("半径不能小于0");
        }
        return Math.PI * Math.pow(radius, 2);
    }

    public static double calculateTriangleArea(double base, double height) {
        if (base < 0 || height < 0) {
            throw new IllegalArgumentException("底边和高不能小于0");
        }
        return 0.5 * base * height;
    }

    public static double calculateRectangleArea(double length, double width) {
        if (length < 0 || width < 0) {
            throw new IllegalArgumentException("长度和宽度不能小于0");
        }
        return length * width;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double radius = scanner.nextDouble();
        double circleArea = calculateCircleArea(radius);
        System.out.println("圆的面积：" + circleArea);

        double base = scanner.nextDouble();
        double height = scanner.nextDouble();
        double triangleArea = calculateTriangleArea(base, height);
        System.out.println("三角形的面积：" + triangleArea);

        double length = scanner.nextDouble();
        double width = scanner.nextDouble();
        double rectangleArea = calculateRectangleArea(length, width);
        System.out.println("矩形的面积：" + rectangleArea);
    }
}