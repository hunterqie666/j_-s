import java.util.Scanner;// 需要导入 util 包
import java.util.SplittableRandom;

public class Finishing {
    public static int add(int x ,int y ){
        return x + y;
    }
    public static double add(double x, double y) {
        return x + y;
    }
    public static double add(double x, double y, double z) {
        return x + y + z;
    }

    public static void main(String[] args) {
        add(1,2); // 调用add(int, int)
        add(1.5, 2.5); // 调用add(double, double)
        add(1.5, 2.5, 3.5); // 调用add(double, double, double)
    }
    public static void swap(int a , int b) {
        int tmp = 0;
        tmp = a;
        a = b;
        b = tmp;
        System.out.println(a);
        System.out.println(b);
    }
    public static void main24(String[] args) {
        int a = 10;
        int b = 20;
        swap(a,b);
    }

    public static int Math(int a) {
        int sum = 0;
        for (int i = 1; i <=a ; i++) {
            int ret = 1;
            for (int j = 1; j <= i; j++) {
                ret *= j;
            }
            sum += ret;
        }
        return sum;
    }
    public static void main23(String[] args) {
        int add = Math(5);
        System.out.println(add);
    }



    public static int add1(int a,int b) {
        return a + b;
    }
    public static void main22(String[] args) {
        int a =10,b = 20;
        int sum = add1( a , b);
        System.out.println(sum);
    }

    public static void main21(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入你的姓名：");
        String name = sc.nextLine();
        System.out.println("请输入你的年龄：");
        int age = sc.nextInt();
        System.out.println("请输入你的工资：");
        float salary = sc.nextFloat();
        System.out.println("你的信息如下：");
        System.out.println("姓名: "+name+"\n"+"年龄："+age+"\n"+"工资："+salary);
        sc.close(); // 注意, 要记得调用关闭方法
    }
    public static void main20(String[] args) {
        Scanner ch = new Scanner(System.in);//固定的句型 ch是自己取的名字
        int a = ch.nextInt();//这里主要看输入什么类型就要用相对应的数据类型
        String str = ch.nextLine();
        short b = ch.nextShort();

        while (ch.hasNext()) {
            int d = ch.nextInt();
        }
    }
    public static void main19(String[] args) {
        /*do{
            循环语句;
        }while(循环条件);*/
        //打印 1 - 10
        int num = 1;
        do {
            System.out.println(num);
            num++;
        } while (num <= 10);
    }
    public static void main18(String[] args) {
        int a = 1;
        while (a<=10)
        {//打印1-10的数字
            if (a == 5)
                //break;//这边停止 1 2 3 4
                continue;//终止本次循环后面的代码不在执行 直接进入到while循环的判断部分
            System.out.println(a);
            a++;
        }
    }
    public static void main17(String[] args) {
//1-10之间数字的相加
        int a = 0;
        for (int i = 1; i <= 10; i++) {
            a +=i;
        }
        System.out.println(a);//55
    }
    public static void main16(String[] args) {
       /* while(循环条件){
            循环语句;
        }*/
//1-100的数字
        int a = 1;
        while (a<=100){
            System.out.println(a);
            a++;
        }
    }

    public static void main15(String[] args) {
        /*switch ( 表达式 ){
            case 常量值1：{
                //语句1；
                break;
            }
            case 常量值2：{
                //语句2；
                break;
            }
            default:{
                //当表达式与常量值都不匹配的时候执行次语句
                break;
            }
        }*/
        int day = 1;
        switch(day) {
            case 1:
                System.out.println("星期一");
                break;
            case 2:
                System.out.println("星期二");
                break;
            case 3:
                System.out.println("星期三");
                break;
            case 4:
                System.out.println("星期四");
                break;
            case 5:
                System.out.println("星期五");
                break;
            case 6:
                System.out.println("星期六");
                break;
            case 7:
                System.out.println("星期日");
                break;
            default:
                System.out.println("输入有误");
                break;
        }
    }
    public static void main14(String[] args) {
//if
        /*if ( 布尔语句表达式 ) {
            //语句
        }else if ( 布尔语句表达式 ) {
            //语句
        }
        else  {
            //语句
        }*/
        //判断一个数字是正数，负数，还是零
        int num = 10;
        if (num > 0) {
            System.out.println("正数");
        } else if (num < 0) {
            System.out.println("负数");
        } else {
            System.out.println("0");
        }
//---------------------------------------
        /*if ( 布尔语句表达式 ) {
            //语句
        } else  {
            //语句
        }*/
        //判断一个数字是奇数还是偶数
        int i = 10;
        if (i % 2 == 0) {
            System.out.println("num 是偶数");
        } else {
            System.out.println("num 是奇数");
        }
//--------------------------------------
        /*if ( 布尔语句表达式 ) {
            //语句
        }*/
        int a = 10;
        if (a == 10) {
            System.out.println("hunetr");
        }
    }
    public static void main13(String[] args) {
//顺序结构
        int a = 10;//第一个执行

        System.out.println(a);//10 第二个执行
        System.out.println(++a);//11 第三个执行
        System.out.println(a-=1);//10 第四个执行
    }
    public static void main12(String[] args) {
        int a = 10;
        int b = 20;
        int max = a > b ? a : b;
        //a>b是false 所以执行b-->b=20-->max=20
        System.out.println(max);

            boolean flag =(true==true) ? (true==true)?false:true : false;//使用
//布尔表达式1？表达式2：表达式3 使用的方式结果和C语言类似 只不过一个是0/1表达真假，一个数true和false表达真假
            System.out.println((true==true) ? (true==true)?false:true : false);
            //第一个(true==true)是布尔表达式 为真执行(true==true)?false:true
            //布尔表达式1？：(true==true) 表达式2： (true==true)?false:true  表达式三：false
    }
    public static void main11(String[] args) {
        int a = 5;

        System.out.println(a<<1);//10
        System.out.println(a>>1);//20
        System.out.println(a>>>1);//2

    }
    public static void main10(String[] args) {
//位运算符

        int a = 10;
        int b = 20;

        System.out.println(a & b);//0
        System.out.println(a | b);//30
        System.out.println(a ^ b);//30

        System.out.println(~a);//-11
        /*System.out.println(10 > 20 & 10 / 0 == 0); // 程序抛出异常
        System.out.println(10 < 20 | 10 / 0 == 0); // 程序抛出异常*/
    }
    public static void main9(String[] args) {
//逻辑运算
        int a= 1;
        int b =1;
        //System.out.println(a&&b);// 报错 两边要是布尔表达式-->输出结果要是true和false
        //&& 逻辑与（短路与）
        // 其使用的方式结果和C语言类似 只不过一个是0/1表达真假，一个数true和false表达真假
        System.out.println(a==1&&b==1);
        System.out.println(a==2&&b==2);

        //|| 逻辑与 其使用的方式结果和C语言类似 只不过一个是0/1表达真假，一个数true和false表达真假
        //两边要是布尔表达式-->输出结果要是true和false
        System.out.println(a==1||b==2);
        System.out.println(a==2||b==1);

        //! 逻辑非 其使用的方式结果和C语言类似 只不过一个是0/1表达真假，一个数true和false表达真假
        // 右边要是布尔表达式-->输出结果要是true和false
        System.out.println(!(a==1));
    }
    public static void main8(String[] args) {
//关系运算符
        int a = 10;
        int b = 20;

        System.out.println(a == b);//false
        System.out.println(a != b);//true
        System.out.println(a >= b);//false
        System.out.println(a <= b);//true
        System.out.println(a > b);//false
        System.out.println(a < b);//true
    }
    public static void main7(String[] args) {
//++    --
        int a = 10;
        ++a;
        System.out.println(a++);//11
        System.out.println(a);//12
        //--和++使用效果一样只不过一个是 +1 一个是-1
    }
    public static void main6(String[] args) {
//增量运算符
        int a = 10;
        a += 2;
        System.out.println(a);//12

        a *= a + 10;
        System.out.println(a);//264
        //a = a * (a + 10)

        a *= 2;
        System.out.println(a);//24

        a -= 2;
        System.out.println(a);//22
        a /= 2;
        System.out.println(a);//11

        a %= 2;
        System.out.println(a);//1
    }
    public static void main5(String[] args) {
//基本四则运算符
        System.out.println((float)5/2);//2.5
        System.out.println(5/(float)2);//2.5
        //5.0/2-->小数部分就可以接收  5/2.0-->同理
        System.out.println((float)(5/2));//2.0

        System.out.println(10%3);//1
        System.out.println(-10%3);//-1
        System.out.println(10%-3);//1
        System.out.println(-10%-3);//-1
        //简便来看就是看前面的被除数是否为负数.

        //做除法和取模时，右操作数不能为0
        System.out.println(0 / 1);//报错
        System.out.println(1 % 0);//报错
    }
    public static void main4(String[] args) {
        int a = 10;
        int b = 20;

        System.out.println(a + b);//30
        System.out.println(a - b);//-10
        System.out.println(a * b);//200
        System.out.println(a / b);//0  要的是整数部分
        System.out.println(a % b);//10 %取模相当于取的是余数部分

        System.out.println(3.5 % 2.5);//1.0
    }
    public static void main3(String[] args) {
//String 转成 int
        String str1 = "123";
        String str2 = "456";
        int a = Integer.parseInt(str1);
        System.out.println(a);

        int c = Integer.valueOf(str1);//value是调用parseint parseint的是value的底层。
        System.out.println(str1+1);//1231

        int b = Integer.parseInt(str2);
        System.out.println(b);
        //字符串里面的是数字才能转，字母不能.

    }
    public static void main2(String[] args) {
// int 转成 String
        //方法1
        int num = 10;
        String str1 = num + "";//利用字符串+基本数据类型==字符串
        //方法2
        String str2 = String.valueOf(num);//通过.号来调用类当中的方法

        System.out.printf("%s\n",str1);//10 进一步证实其int转化为String
        System.out.printf("%s",str2);//10
    }
    public static void main1(String[] args) {
        /*String a = "a";
        String b = "美";
        System.out.println(a);
        System.out.println(b);
        System.out.println(a + b);// a+b表示：将a和b进行拼接*/
//字符串+字符串是两个字符串直接相加
        int a = 10,b = 20;
        System.out.println(a+b);//30
        System.out.println("a = "+ a);//a=10
        System.out.println("b = "+ b);//b=20
        System.out.println("a + b = "+ a+b);//a + b = 1020
        System.out.println("a + b = "+ (a+b));//a + b = 30
        System.out.println(a+b+"是a+b的和");//30是a+b的和
    }
}