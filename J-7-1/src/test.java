import java.util.Random;
import java.util.Scanner;

public class test {
   
    public static void main16(String[] args) {
        //猜数字小游戏
        Scanner scan = new Scanner(System.in);//输入

        Random random = new Random();
        int  rand = random.nextInt(100);//自动生成一个0-99的数字 不包含100
        //int  rand = random.nextInt(100)+50;//自动生成一个50-99的数字 不包含100
        //System.out.println(rand);

        while(true){
            System.out.println("请输入你要猜的数字：");
            int a = scan.nextInt();
            if(a > rand){
                System.out.println("猜大了！");
            }else if(a == rand){
                System.out.println("猜对了！");
                break;
            }else{
                System.out.println("猜小了！");
            }

        }
    }
    public static void main15(String[] args) {
        Scanner scan = new Scanner(System.in);

        while(scan.hasNextInt()){//循环输入 Ctrl+D 结束
            int n = scan.nextInt();
            System.out.println("n = "+n);
        }
    }
    public static void main14(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("请输入年龄");
        int n =scan.nextInt();
        System.out.println(n);
        //此时上面输入玩之后  回车 会被下面读取到 就类似于C语言的输入缓冲区一样
        System.out.println("请输入姓名");
        String a = scan.nextLine();//遇到空格不会停止  1
        System.out.println(a);

        //----------------------------

        String a1 = scan.nextLine();//遇到空格不会停止  1
        System.out.println(a1);

        String b1 = scan.next();//遇到空格就停止
        System.out.println(b1);//当b1读取之后 b2会把空格后面的内容读取
        String b2 = scan.next();//遇到空格就停止
        System.out.println(b2);
    }
    public static void main13(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n =scan.nextInt();
        System.out.println(n);
    }
    public static void main12(String[] args) {
        System.out.println("输出换行");
        System.out.print("输出不换行");
        System.out.printf("%s\n"," 和C语言一样 格式化输出 !");
    }
    public static void main11(String[] args) {
        int i=0;
        do {
            System.out.println(i);
            i++;
        }while (i<=10);
    }
    public static void main10(String[] args) {
        int a=1,ret=1,sum=0;
        for (int i = 0; i < 6; i++) {
            ret=1;
            for(a=1;a<i;a++){
                ret*=a;
            }
            sum+=ret;
        }
        System.out.println(sum);
    }
    public static void main9(String[] args) {
        int a=1,ret=1;
        for(a=1;a<10;a++){//for(表达式1；表达式2（布尔表达式）；表达式3)//跟C语言类似
            ret*=a;
        }
    }
    public static void main8(String[] args) {
        int a=1;
        while(a<=100){
            if(a%3!=0||a%5!=0){
                a++;
                continue;
            }
            System.out.println(a);
            a++;
        }
    }
    public static void main7(String[] args) {
        int a=1;
        while(a<=10){
            if(a==5){//break和continue的效果跟C语言一样。
                continue;//死循环
                //break;
            }
            System.out.println(a);
            a++;
        }
    }
    public static void main6(String[] args) {
        int a=1;
        int sum=0;
        while(a<=100){//参数是布尔型
            sum+=a;
            a++;
        }
        System.out.println(sum);
    }
    public static void main5(String[] args) {
     int a=1;//跟C语言一模一样
     switch (a){//long double float boolean 都不能当做switch的参数
         case 1://字符串可以的 String
             System.out.println("1");
             break;
     }
    }
    public static void main4(String[] args) {
        int a=10;
        if(a==10);{
            //if后面不能有逗号 有了不管真假都会执行后面的句子
        }
    }
    public static void main3(String[] args) {
        //闰年
        int year = 2000;
        if (year % 100 == 0) {
// 判定世纪闰年
            if (year % 400 == 0) {
                System.out.println("是闰年");
            } else {
                System.out.println("不是闰年");
            }
        } else {
// 普通闰年
            if (year % 4 == 0) {
                System.out.println("是闰年");
            } else {
                System.out.println("不是闰年");
            }
        }
    }
    public static void main2(String[] args) {
        int a=10;
        if(a%2!=0){
            System.out.println("偶数");
        }else{
            System.out.println("奇数");
        }
    }
    public static void main1(String[] args) {
        int a = 10;//和C语言类似就是else的位置 然后就是a=10会报错。
        if (a == 10) {//a=10 在java里面是错的
            System.out.println(a);
        } else if (a == 11) {
            System.out.println("a!=10");
        } else {
            System.out.println("11");
        }
    }
}
