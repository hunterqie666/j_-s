import java.util.Scanner;

public class GamePlatform {
    private Computer mComputer;  // 电脑实例
    private Player mPlayer;          // 玩家实例

    private void beginningShow() {
        System.out.println("—————欢迎进入游戏世界—————");
        System.out.println("**********************");
        System.out.println("********猜拳开始********");
        System.out.println("**********************");
    }

    private void gameStart() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("出拳规则：1.石头   2.剪刀   3.布");
        System.out.print("请输入玩家名字：");
        String playerName = scanner.nextLine();
        mPlayer.setPlayerName(playerName);

        System.out.print("请选择对手<1-曹操 2-刘备 3-孙权>:");
        int choice = scanner.nextInt();
        if(choice == 1) {
            mComputer.setComputerName("曹操");
        }else if(choice == 2){
            mComputer.setComputerName("刘备");
        }else {
            mComputer.setComputerName("孙权");
        }
    }

    private void gameProceed() {
        Scanner scanner = new Scanner(System.in);
        while(true) {
            System.out.println("请输入你要出的拳：1.石头   2.剪刀   3.布");
            int choice = scanner.nextInt();
            String playerMethod = mPlayer.getPlayerMethod(choice);
            String computerMethod = mComputer.getComputerMethod();

            System.out.println("玩家出拳：" + playerMethod);
            System.out.println(mComputer.getComputerName() + "出拳：" + computerMethod);


            if ((playerMethod.equals("剪刀") && computerMethod.equals("布")) ||
                    (playerMethod.equals("石头") && computerMethod.equals("剪刀")) ||
                    (playerMethod.equals("布") && computerMethod.equals("石头"))) {
                System.out.println("玩家胜利！");
                mPlayer.winTimes++;
            } else if (playerMethod.equals(computerMethod)) {
                System.out.println("平局！");
            } else {
                System.out.println(mComputer.getComputerName() + "胜利！");
                mComputer.winTimes++;
            }


            System.out.println("是否继续游戏？（输入y继续，输入n退出）");
            char continueChoice = scanner.next().charAt(0);
            if (continueChoice == 'n') {
                break;
            }
        }
    }

    private void gameEnd() {
        System.out.println("----------结果-------------");
        System.out.println("  *** " + mPlayer.getPlayerName() + " : " + mComputer.getComputerName() + "  ***  ");
        System.out.println("----------比分-------------");
        System.out.println("  ***  " + mPlayer.winTimes + "  :  " + mComputer.winTimes + "   ***   ");

        System.out.println("**************************");
        if (mPlayer.winTimes > mComputer.winTimes) {
            System.out.println("恭喜你获胜！");
        } else if (mPlayer.winTimes < mComputer.winTimes) {
            System.out.println("恭喜你失败");
        } else {
            System.out.println("平局！");
        }
    }

    public void algorithmProceed() {
        mComputer = new Computer();
        mPlayer = new Player();

        beginningShow();
        gameStart();
        gameProceed();
        gameEnd();
    }
}