
public class Player {
    private String playerName;     // 玩家姓名
    private String playerMethod;   // 玩家方法
    static int winTimes = 0;       // 玩家累计赢的次数

    public String getPlayerName() {
        return playerName;
    }
    public void setPlayerName(String Name) {
        playerName = Name;
    }
    public String getPlayerMethod(int num) {
        if (num == 0) {
            playerMethod = "剪刀";
        } else if (num == 1) {
            playerMethod = "石头";
        } else {
            playerMethod = "布";
        }
        return playerMethod;
    }

}

