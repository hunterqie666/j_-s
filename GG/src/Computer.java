
public class Computer {
    private String computerName;     // 电脑名称
    private String computerMethod;   // 电脑出拳方法
    static int winTimes = 0;         // 电脑累计赢的次数


    public void setComputerName(String name){
        computerName = name;
    }
    public String getComputerName() {
        return computerName;
    }
    public String getComputerMethod() {
        int randomNum = (int)(Math.random() * 3);
        if (randomNum == 0) {
            computerMethod = "剪刀";
        } else if (randomNum == 1) {
            computerMethod = "石头";
        } else {
            computerMethod = "布";
        }
        return computerMethod;
    }
}
